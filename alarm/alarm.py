#! /usr/bin/env python3.7

from contextlib import asynccontextmanager
import asyncio
from datetime import datetime
import random
import logging
from functools import partial
import sys

from mpd.asyncio import MPDClient, ConnectionError
from playsound import playsound

from .config import *  # noqa

MPD_STATUS_PLAY = 'play'
MPD_STATUS_PAUSE = 'pause'
MPD_STATUS_STOP = 'stop'


logger = logging.getLogger('alarm')
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)


@asynccontextmanager
async def mpd_connect():
    client = MPDClient()
    client.timeout = 10
    client.idletimeout = None
    try:
        await client.connect(MPD_HOST, MPD_PORT)
        logger.debug('Client connected to {}'.format(MPD_HOST))

        yield client

        logger.debug('Client disconnecting')
        # client.close()
        client.disconnect()
    except ConnectionError as e:
        logger.error(e)


@asynccontextmanager
async def set_mpd_output(client, output):
    actual_outputs = await client.outputs()
    changed_outputs = set()
    for output in actual_outputs:
        logger.debug('Output {} is {}'.format(
            output['outputname'],
            'enabled' if output['outputenabled'] == '1' else 'disabled'))
        if output['outputname'] == MPD_OUTPUT:
            if output['outputenabled'] == '0':
                logger.debug('Enabling output {} ({})'
                             .format(output['outputname'], output['outputid']))
                await client.enableoutput(output['outputid'])
                changed_outputs.add(output['outputid'])
        else:
            if output['outputenabled'] == '1':
                logger.debug('Disabling output {} ({})'
                             .format(output['outputname'], output['outputid']))
                await client.disableoutput(output['outputid'])
                changed_outputs.add(output['outputid'])

    yield
    for outputid in changed_outputs:
        logger.debug('Resetting status of output {}'.format(outputid))
        await client.toggleoutput(outputid)


async def stop_alarm(client):
    await asyncio.sleep(MPD_PLAY_TIME.seconds)
    song = await client.currentsong()
    logger.debug('Playing {}, stopping at its end'.format(song['title']))
    status = await client.status()
    if status['state'] == MPD_STATUS_PLAY:
        client.idle('player')
        logger.debug('Song should be over. Quitting')
        await client.stop()


async def play_alarm(client):
    async with set_mpd_output(client, MPD_OUTPUT):
        # choosing a random album
        logger.debug('listing albums')
        albums = list(await client.list('album'))
        album_name = random.choice(albums)
        logger.info('Playing album {}'.format(album_name))
        album_songs = await client.find('album', album_name)
        first_song = album_songs[0]
        logger.info('Artist is {}'.format(first_song['albumartist']))
        first_id = None
        for song in album_songs:
            logger.debug('Adding {} - {}'.format(song['track'],
                                                 song['title']))
            new_id = await client.addid(song['file'])
            if first_id is None:
                first_id = new_id

        logger.debug('Playing music')
        await client.playid(first_id)
        await stop_alarm(client)


async def play_warmed_up_alarm(client):
    await asyncio.sleep(MPD_WARMUP_TIME.seconds)
    await play_alarm(client)


async def amain():
    logger.debug('Here we go!')
    async with mpd_connect() as client:
        status = await client.status()
        if status['state'] != MPD_STATUS_PLAY:
            # let's groove!
            if MPD_WARMUP_TIME is not None:
                alarm = asyncio.ensure_future(play_warmed_up_alarm(client))
                play_cmd = list(map(partial(str.format, file='void.ogg'),
                                    SOUND_PLAY_CMD))
                proc = await asyncio.create_subprocess_exec(
                    *play_cmd,
                    stdout=asyncio.subprocess.PIPE,
                    stderr=asyncio.subprocess.PIPE)
                await proc.communicate()
                await alarm
            else:
                await play_alarm(client)


def main():
    asyncio.run(amain())


if __name__ == '__main__':
    main()
