#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='MPD alarm',
    version='0.1.0',
    author='Gordon',
    description='Simple script to make an alarm clock out of an MPD setup',
    include_package_data=True,
    packages=find_packages(),
    url='https://framagit.org/gordon/mpd-alarm',
    classifiers=[
        'Programming Language :: Python',
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Framework :: AsyncIO',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU Affero General Public License v3 or '
        'later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Home Automation',
        'Topic :: Multimedia :: Sound/Audio :: Players',
        ],
    install_requires=[
        'python_mpd2==1.0.0',
        ],
    entry_points = {
        'console_scripts': [
            'mpd-alarm = alarm:main',
            ],
        },
    license='AGPLv3'
    )
